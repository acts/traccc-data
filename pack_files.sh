#!/bin/bash

set -u
set -e

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
cd $SCRIPT_DIR

files=$(find . -mindepth 1 -maxdepth 1 -type d -not -name ".git")

find . -type f -name "*.tar" -delete
find . -type f -name "*.tar.gz" -delete

function getsize() {
	du -sm $1 | awk '{print $1}'
}

for file in $files; do
	size=$(getsize $file)

	echo "Packing $file [${size}M]"
	tar czf ${file}.tar.gz $file
	echo "-> $(getsize ${file}.tar.gz)M"
	rm -rf ${file}

done
